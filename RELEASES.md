# Releases

This readme is for the descriptions for the Docker Hub releases.

## v1.6.0

- [#28] - Kubernetes Docker config changes

## v1.5.0

- First Docker release for client nodes. Used with go-naka:1.5.0.
